export default class Intervention {
  readonly timeStart: number;

  readonly timeEnd: number;

  readonly duration: number;

  readonly talker: string;

  readonly nextTalker: string | null;

  readonly previousTalker: string | null;

  readonly isInterrupted: boolean;

  readonly isInterrupting: boolean;

  constructor(
    timeStart: number,
    timeEnd: number,
    talker: string,
    isInterrupting: boolean,
    previousTalker: string | null,
    isInterruptedBy: boolean,
    nextTalker: string | null,
  ) {
    this.timeStart = timeStart;
    this.timeEnd = timeEnd;
    this.duration = this.timeEnd - this.timeStart;
    this.talker = talker;
    this.previousTalker = previousTalker;
    this.isInterrupting = isInterrupting;
    this.nextTalker = nextTalker;
    this.isInterrupted = isInterruptedBy;
  }
}
