import * as _ from 'lodash';
import { fromPairs } from 'lodash';
import Intervention from '@/models/intervention';
import TalkerCategory from '@/models/talker-category';
import Gender from '@/models/gender';

export class Talker {
  readonly name: string;

  readonly index: number;

  readonly category: TalkerCategory = new TalkerCategory();

  constructor(name: string, index = -1) {
    this.name = name;
    this.index = index;
  }
}

export default class Meeting {
  readonly interventions: Intervention[];

  readonly duration: number;

  readonly talkers: { [key: string]: Talker };

  constructor(
    interventions: Intervention[],
  ) {
    this.interventions = interventions;
    this.duration = _.chain(interventions)
      .map((i) => i.timeEnd)
      .max()
      .value();

    this.talkers = _.chain(interventions)
      .map((i) => i.talker)
      .uniq()
      .orderBy()
      .map((name: string, index: number) => [name, new Talker(name, index)])
      .fromPairs()
      .value();
  }

  isEmpty(): boolean {
    return this.interventions.length === 0;
  }

  getTalkerNames(): string[] {
    return _.chain(this.talkers)
      .map((t) => t.name)
      .orderBy()
      .value();
  }

  getTalker(talkerName: string): Talker {
    return this.talkers[talkerName];
  }

  /**
   * build a map talker name -> total talking duration
   */
  talkerTotalContribution(): { [key: string]: number } {
    return _.chain(this.interventions)
      .groupBy((intervention: Intervention) => intervention.talker)
      .map((interventions: Intervention[], name) => [name, _.chain(interventions).map('duration').sum().value()])
      .fromPairs()
      .value();
  }

  setTalkerGender(talkerName: string, gender: Gender) {
    this.getTalker(talkerName).category.gender = gender;
  }

  getTalkerGender(talkerName: string): Gender | undefined {
    return this.getTalker(talkerName).category.gender;
  }

  getTalkerGenders(): { [key: string]: string | undefined } {
    return _.chain(this.talkers)
      .map((talker: Talker) => [talker.name, talker.category.gender])
      .fromPairs()
      .value();
  }

  /** returns true if all talker have been set a gender */
  isGenderFullyAttributed(): boolean {
    return _.chain(this.talkers)
      .filter((t) => !t.category.gender)
      .size()
      .value() === 0;
  }

  /** build a map from gender -> intervention duration ratio */
  genderContributionRatio(): { [key: string]: number } {
    if (this.duration === 0) {
      return {};
    }
    const totalByGender = _.chain(this.interventions)
      .groupBy((intervention: Intervention) => this.getTalkerGender(intervention.talker))
      .map((interventions: Intervention[], gender: Gender | undefined) => [
        gender,
        _.chain(interventions)
          .map((intervention) => intervention.timeEnd - intervention.timeStart)
          .sum()
          .value(),
      ])
      .fromPairs()
      .value();

    const countByGender = _.chain(this.interventions)
      .map((intervention: Intervention) => intervention.talker)
      .uniq()
      .countBy((talker: string) => this.getTalkerGender(talker))
      .value();

    const weightedByGender = _.chain(totalByGender)
      .map((total, gender) => [gender, total / countByGender[gender]])
      .value();

    const total = _.chain(weightedByGender)
      .map(([gender, w]) => w)
      .sum()
      .value();

    return _.chain(weightedByGender)
      .map(([gender, w]) => [gender, (w as number) / total])
      .fromPairs()
      .value();
  }
}
