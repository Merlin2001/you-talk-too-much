import Vue from 'vue';
import Vuex from 'vuex';
import Meeting from '@/models/meeting';
import buildMeeting from '@/services/meeting-builder';
import VttParser from '@/services/vtt-parser';
import Gender from '@/models/gender';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    meeting: new Meeting([]),
    isSettingsVisible: false,
    talkerGenders: {} as { [key: string]: string | undefined },
    genderContributionRatio: {} as { [key: string]: number},
    talkerTotalContribution: {} as { [key: string]: number},
  },
  mutations: {
    setMeeting(state, meeting: Meeting) {
      state.meeting = meeting;
      state.talkerGenders = meeting.getTalkerGenders();
      state.genderContributionRatio = meeting.genderContributionRatio();
      state.talkerTotalContribution = meeting.talkerTotalContribution();
    },
    setTalkerGender(state, tg: { talkerName: string, gender: Gender }) {
      state.meeting.setTalkerGender(tg.talkerName, tg.gender);
      state.talkerGenders = state.meeting.getTalkerGenders();
      state.genderContributionRatio = state.meeting.genderContributionRatio();
      state.talkerTotalContribution = state.meeting.talkerTotalContribution();
    },
    showSettings(state) {
      state.isSettingsVisible = true;
    },
    hideSettings(state) {
      state.isSettingsVisible = false;
    },
  },
  actions: {
    loadVtt({ commit }, content: string) {
      const vttRecords = new VttParser(content).records();
      const meeting = buildMeeting(vttRecords);
      commit('setMeeting', meeting);
    },
  },
});
export default store;
