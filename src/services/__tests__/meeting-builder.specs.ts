import { VttRecord } from '@/models/vtt-record';
import Intervention from '@/models/intervention';
import { packInterventions, packVttRecords } from '@/services/meeting-builder';

describe('meeting-builder', () => {
  describe('packVttRecords', () => {
    it('empty should return empty', () => {
      const got = packVttRecords([]);

      expect(got).toEqual([]);
    });

    describe('single talker', () => {
      it('a succession of close enough record packed into on', () => {
        const records = [
          new VttRecord(0, 2, 'A'),
          new VttRecord(2.1, 5, 'A'),
          new VttRecord(5.2, 7, 'A'),
        ];
        const got = packVttRecords(records);

        const expected = [
          new VttRecord(0, 7, 'A'),
        ];
        expect(got).toEqual(expected);
      });
      it('paused should be broken into multiple records', () => {
        const records = [
          new VttRecord(0, 2, 'A'),
          new VttRecord(2.1, 5, 'A'),
          new VttRecord(6, 7, 'A'),
          new VttRecord(7.4, 9, 'A'),
          new VttRecord(11, 12, 'A'),
        ];
        const got = packVttRecords(records);

        const expected = [
          new VttRecord(0, 5, 'A'),
          new VttRecord(6, 9, 'A'),
          new VttRecord(11, 12, 'A'),
        ];
        expect(got).toEqual(expected);
      });

      it('short packed records should be removed', () => {
        const records = [
          new VttRecord(0, 2, 'A'),
          new VttRecord(2.1, 2.3, 'A'), // to pack
          new VttRecord(6, 6.3, 'A'), // too short
          new VttRecord(7.4, 7.7, 'A'), // too short
          new VttRecord(11, 12, 'A'),
        ];
        const got = packVttRecords(records);

        const expected = [
          new VttRecord(0, 2.3, 'A'),
          new VttRecord(11, 12, 'A'),
        ];
        expect(got).toEqual(expected);
      });
    });

    describe(('multiple talkers'), () => {
      it('short packed records should be removed', () => {
        const records = [
          new VttRecord(0, 2, 'A'),
          new VttRecord(2.1, 2.3, 'A'), // to pack
          new VttRecord(6, 6.3, 'A'), // too short
          new VttRecord(7.4, 7.7, 'A'), // too short
          new VttRecord(11, 12, 'A'),
          new VttRecord(11.1, 13, 'B'),
        ];
        const got = packVttRecords(records);

        const expected = [
          new VttRecord(0, 2.3, 'A'),
          new VttRecord(11, 12, 'A'),
          new VttRecord(11.1, 13, 'B'),
        ];
        expect(got).toEqual(expected);
      });
    });
  });

  describe('packInterventions', () => {
    it(('silence'), () => {
      const records: VttRecord[] = [];

      const interventions = packInterventions(records);

      const expected: Intervention[] = [];
      expect(interventions).toEqual(expected);
    });

    it(('monologue'), () => {
      const records = [
        new VttRecord(0, 2, 'A'),
        new VttRecord(3, 5, 'A'),
        new VttRecord(6, 7, 'A'),
        new VttRecord(7, 10, 'A'),
      ];

      const interventions = packInterventions(records);

      const expected = [
        new Intervention(0, 10, 'A', false, null, false, null),
      ];
      expect(interventions).toEqual(expected);
    });

    it(('Ever changing'), () => {
      const records = [
        new VttRecord(0, 2, 'A'),
        new VttRecord(3, 5, 'B'),
        new VttRecord(6, 7, 'A'),
        new VttRecord(7, 10, 'C'),
      ];

      const interventions = packInterventions(records);

      const expected = [
        new Intervention(0, 2, 'A', false, null, false, 'B'),
        new Intervention(3, 5, 'B', false, 'A', false, 'A'),
        new Intervention(6, 7, 'A', false, 'B', false, 'C'),
        new Intervention(7, 10, 'C', false, 'A', false, null),
      ];
      expect(interventions).toEqual(expected);
    });

    it(('multiple short'), () => {
      const records = [
        new VttRecord(0, 0.9, 'A'),
        new VttRecord(1, 2, 'A'),
        new VttRecord(3, 3.5, 'B'),
        new VttRecord(4, 4.6, 'B'),
        new VttRecord(4.7, 5, 'B'),
        new VttRecord(6, 7, 'A'),
        new VttRecord(7, 8, 'C'),
        new VttRecord(8.2, 9, 'C'),
        new VttRecord(9.2, 10, 'C'),
      ];

      const interventions = packInterventions(records);

      const expected = [
        new Intervention(0, 2, 'A', false, null, false, 'B'),
        new Intervention(3, 5, 'B', false, 'A', false, 'A'),
        new Intervention(6, 7, 'A', false, 'B', false, 'C'),
        new Intervention(7, 10, 'C', false, 'A', false, null),
      ];
      expect(interventions).toEqual(expected);
    });

    it(('interrupting'), () => {
      const records = [
        new VttRecord(0, 2, 'A'),
        new VttRecord(3, 5, 'B'),
        new VttRecord(4.5, 7, 'A'),
        new VttRecord(7, 10, 'C'),
      ];

      const interventions = packInterventions(records);

      const expected = [
        new Intervention(0, 2, 'A', false, null, false, 'B'),
        new Intervention(3, 5, 'B', false, 'A', true, 'A'),
        new Intervention(4.5, 7, 'A', true, 'B', false, 'C'),
        new Intervention(7, 10, 'C', false, 'A', false, null),
      ];
      expect(interventions).toEqual(expected);
    });
  });
});
