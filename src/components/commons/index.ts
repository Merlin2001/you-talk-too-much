import * as _ from 'lodash';
import { scaleLinear } from 'd3';
import { Talker } from '@/models/meeting';

/**
 * build a function that given a talker name, will return the pixel position
 * @param talkers
 */
// eslint-disable-next-line import/prefer-default-export
export function talkerScale(talkers: Talker[], rangeLength: number) {
  const talkerDict = _.chain(talkers)
    .map((t) => [t.name, t])
    .fromPairs()
    .value();

  const incLength = rangeLength / _.size(talkers);
  const y = scaleLinear()
    .domain([0, _.size(talkers)])
    .range([incLength / 2, rangeLength]);
  return (name: string) => y(talkerDict[name].index);
}
