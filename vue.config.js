let publicPath = '';
if (process.env.CI_PROJECT_NAME) {
  publicPath = `/${process.env.CI_PROJECT_NAME}`;
}
module.exports = {
  publicPath,
};
